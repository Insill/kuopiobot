'''
Created on 25.8.2020

@author: Insill
'''
import configparser
import os
import telegram
import schedule
import parse
import time
import sys
import connectionchecker
import common_log

this_folder = os.path.dirname(os.path.abspath(__file__))
config_filepath = os.path.join(this_folder, "config.ini")
logger = common_log.CommonLog("bot",20).get_logger()
config = configparser.ConfigParser(interpolation=None)
config.read(config_filepath)
bot = telegram.Bot(token=config.get("API Tokens", "telegram"))


def post_message(message):
    bot.send_message(chat_id=config.get("Configuration", "channel_id"), disable_web_page_preview=True, text=message)


def get_handle_post():
    data = parse.get_data()
    interval_minutes = int(config.get("Configuration", "interval_minutes"))
    if data:
        logger.info("{} total entries".format(len(data)))
        post_data(data, interval_minutes)
    else:
        logger.info("No new entries")


def post_data(data, interval_minutes):
    for entry in data:
        post_message(entry)
        time.sleep(interval_minutes * 30)
        

def main():
    conn = connectionchecker.ConnectionChecker()
    interval_hours = int(config.get("Configuration", "interval_hours"))
    logger.info("Bot started")
    schedule.every(interval_hours).hours.do(conn.is_connected)
    schedule.every(interval_hours).hours.do(get_handle_post)

    while True:
        try: 
            schedule.run_pending()
            time.sleep(1)
        except:
            logger.exception("Exception")
            continue


main()
