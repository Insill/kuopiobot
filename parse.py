'''
Created on 25.8.2020

@author: Insill
'''
import configparser
import feedparser
import os
import common_log
from datetime import datetime

this_folder = os.path.dirname(os.path.abspath(__file__))
config_filepath = os.path.join(this_folder, "config.ini")
config = configparser.ConfigParser(interpolation=None)
logger = common_log.CommonLog("parse",20).get_logger()
config.read(config_filepath)


def get_data():
    events1 = get_data_kuopio()
    events2 = get_data_peto()
    data = events1 + events2
    return data


def get_data_kuopio():
    feed = feedparser.parse(config.get("RSS Feeds", "kuopio")) 
    events = []
    date_latest = None
    date_entry = None
    
    for entry in reversed(feed.entries):
        date_latest = datetime.strptime(config.get("Configuration", "date_latest_kuopio"), "%d-%m-%Y %H:%M:%S")
        date_entry = datetime.strptime(entry.published, "%Y-%m-%dT%H:%M:%S%z").replace(tzinfo=None)
         
        if date_entry > date_latest:
            title = entry.title
            summary = entry.summary
            link = entry.id
            event = compose_event(title, datetime.strftime(date_entry,"%d-%m-%Y %H:%M:%S"), summary, link)
            events.append(event)
            update_date_latest(datetime.strftime(date_entry, "%d-%m-%Y %H:%M:%S"), "date_latest_kuopio")

    return events

def get_data_peto():
    feed = feedparser.parse(config.get("RSS Feeds", "peto"))
    events = []
    for entry in reversed(feed.entries):
        date_latest = datetime.strptime(config.get("Configuration", "date_latest_peto"), "%d-%m-%Y %H:%M:%S")
        date_entry = datetime.strptime(entry.published, "%a, %d %b %Y %H:%M:%S %z").replace(tzinfo=None)
        
        if date_entry > date_latest and entry.title[0:6] == "Kuopio":
            summary = entry.summary[27:]
            title = ""
            link = ""
            event = compose_event(title, datetime.strftime(date_entry,"%d-%m-%Y %H:%M:%S"), summary, link)
            events.append(event) 
            update_date_latest(datetime.strftime(date_entry, "%d-%m-%Y %H:%M:%S"), "date_latest_peto")
            
    return events

def compose_event(title, date, summary, link):
    event = ""
    event += "{}\n".format(title)
    event += "\U0001F4C5 {}\n".format(date)
    event += "{}\n".format(summary)
    event += "\U0001F517 {}".format(link)
    logger.debug(event)
    return event


def update_date_latest(date, config_var):
    config.set("Configuration", config_var, date)
    configfile = open("config.ini", "w")
    config.write(configfile)
    configfile.close()
